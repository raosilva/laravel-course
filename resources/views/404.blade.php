@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('404') }}</div>

                <div class="card-body">
                   <div class="alert alert-danger" role="alert">
                            <h1>404</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
